import "./topics/01-basic-types";
import "./topics/02-object-interface";
import "./style.css";

document.querySelector<HTMLDivElement>("#app")!.innerHTML = `Hello world!`;
